# Life is the Game Test
## Requirements
* [X] Menu with character animations [[#1](https://github.com/Steback/LitGame/pull/1)]
* [X] Player FPV(First Person View) and scene transition [[#2](https://github.com/Steback/LitGame/pull/2)]
* [X] Game UI [[#4](https://github.com/Steback/LitGame/pull/4)]
* [X] Parabolic firing weapon [[#7](https://github.com/Steback/LitGame/pull/7)]
* [X] Attraction firing weapon [[#8](https://github.com/Steback/LitGame/pull/8)]
* [X] Bouncing firing weapon [[#9](https://github.com/Steback/LitGame/pull/9)]

## Dependencies
* [Unity 2020.3.14f1](https://unity.com/releases/editor/whats-new/2020.3.14)
